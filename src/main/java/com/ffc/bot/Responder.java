package main.java.com.ffc.bot;

import main.java.com.ffc.bot.states.general.GeneralState;
import main.java.com.ffc.bot.states.order.OrderState;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Responder extends TelegramLongPollingBot {

    Logger LOGGER = Logger.getLogger(Responder.class.getName());

    @Override
    public void onUpdateReceived(Update update) {

        try {


            SendMessage response;

            String chatId = getChatId(update);

            if (!MongoDB.userExists(chatId)) {
                MongoDB.insertNewUserId(chatId);
                response = new GeneralState().getResponse(update);
                sendApiMethod(response);
                return;
            }

            String currentState = MongoDB.getFieldValue(MongoDB.STATE, chatId);

            if (currentState.equalsIgnoreCase(State.GENERAL.toString())) {
                response = new GeneralState().getResponse(update);
                sendApiMethod(response);
                return;
            }

            if (currentState.equalsIgnoreCase(State.ORDER_STATUS.toString())) {
                response = new OrderState().getResponse(update);
                sendApiMethod(response);
                return;
            }

            LOGGER.log(Level.SEVERE, "No response could be determined!", update);

        } catch (TelegramApiException telegramApiException) {
            telegramApiException.printStackTrace();
        }
    }

    @Override
    public String getBotToken() {
        return Bot.BOT_TOKEN;
    }

    @Override
    public String getBotUsername() {
        return Bot.BOT_USERNAME;
    }

    private static String getChatId(Update update) {
        if (update.hasMessage()) {
            return String.valueOf(update.getMessage().getChatId());
        }

        if (update.hasCallbackQuery()) {
            return String.valueOf(update.getCallbackQuery().getMessage().getChatId());
        }

        throw new IllegalStateException("The update type could not be determined!");
    }
}
