package main.java.com.ffc.bot;

import main.java.com.ffc.bot.states.order.OrderStatus;

import java.util.HashMap;
import java.util.Map;

public class OrderDetailsDAOImpl {

    public static Map<String, OrderStatus> orders = new HashMap<>();

    static {
        orders.put("123456", OrderStatus.IN_PROGRESS);
        orders.put("343434", OrderStatus.DELIVERED);
        orders.put("234343", OrderStatus.PENDING);
    }
}
