package main.java.com.ffc.bot.states.order;

public enum OrderStatus {
    PENDING,IN_PROGRESS,DELIVERED
}
