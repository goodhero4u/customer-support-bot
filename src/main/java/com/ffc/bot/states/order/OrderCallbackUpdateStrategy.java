package main.java.com.ffc.bot.states.order;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

public class OrderCallbackUpdateStrategy implements OrderStrategy{

    @Override
    public SendMessage getResponse(Update update) {
        String chatId = String.valueOf(update.getCallbackQuery().getMessage().getChatId());

        SendMessage response = new SendMessage();
        response.setText("Order number is expected, so please input the order number or tap /start to start over");
        response.setChatId(chatId);

        return response;
    }
}
