package main.java.com.ffc.bot.states.order;

import main.java.com.ffc.bot.MongoDB;
import main.java.com.ffc.bot.OrderDetailsDAOImpl;
import main.java.com.ffc.bot.State;
import main.java.com.ffc.bot.states.general.GeneralTextUpdateStrategy;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

public class OrderTextUpdateStrategy implements OrderStrategy {

    @Override
    public SendMessage getResponse(Update update) {

        String chatId = String.valueOf(update.getMessage().getChatId());

        SendMessage response = new SendMessage();
        response.setChatId(chatId);
        response.setText("Invalid response entered. Please input the order number or tap /start to start over.");

        String textUpdate = update.getMessage().getText().trim();

        if (OrderDetailsDAOImpl.orders.containsKey(textUpdate)) {
            response.setText(OrderDetailsDAOImpl.orders.get(textUpdate).toString());
        }

        if (textUpdate.equalsIgnoreCase("/start")) {
            MongoDB.updateField(MongoDB.STATE, State.GENERAL.toString(),chatId);
            response = new GeneralTextUpdateStrategy().getResponse(update);
        }

        return response;

    }
}
