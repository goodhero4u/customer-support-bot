package main.java.com.ffc.bot.states.order;

import main.java.com.ffc.bot.states.State;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

public class OrderState implements State {

    @Override
    public SendMessage getResponse(Update update) {

        OrderStrategy orderStrategy = null;

        if (update.hasMessage()) {
            orderStrategy = new OrderTextUpdateStrategy();
        }

        if (update.hasCallbackQuery()) {
            orderStrategy = new OrderCallbackUpdateStrategy();
        }

        if (orderStrategy == null) {
            throw new IllegalStateException("The update type could not be determined");
        }

        return orderStrategy.getResponse(update);

    }
}
