package main.java.com.ffc.bot.states.order;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

public interface OrderStrategy {

    SendMessage getResponse(Update update);
}
