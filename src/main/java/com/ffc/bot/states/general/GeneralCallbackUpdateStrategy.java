package main.java.com.ffc.bot.states.general;

import main.java.com.ffc.bot.MongoDB;
import main.java.com.ffc.bot.State;
import main.java.com.ffc.bot.responses.CallBackData;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

import static main.java.com.ffc.bot.responses.CallBackData.CONTACT_HUMAN;
import static main.java.com.ffc.bot.responses.CallBackData.ORDER_STATUS;

public class GeneralCallbackUpdateStrategy implements GeneralStrategy {

    @Override
    public SendMessage getResponse(Update update) {
        String chatId = String.valueOf(update.getCallbackQuery().getMessage().getChatId());

        SendMessage response = new SendMessage();
        response.setText("Order number is expected, so please input the order number or tap /start to start over");
        response.setChatId(chatId);

        String callBackData = update.getCallbackQuery().getData();

        if (callBackData.equalsIgnoreCase(CallBackData.ORDER_STATUS.toString())) {
            response.setText("Please input the order number to get the order status.");
            MongoDB.updateField(MongoDB.STATE, State.ORDER_STATUS.toString(), chatId);
        }

        if (callBackData.equalsIgnoreCase(CallBackData.MORE_INFORMATION.toString())) {
            response.setText("Thank you for your interest in our product. Our company works 24/7 and produces all the necessary equipment straight to your home. Feel free to check our website.");

            // First create the keyboard
            List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();

            //Then we create the buttons' row
            List<InlineKeyboardButton> buttonsRow = new ArrayList<>();

            InlineKeyboardButton websiteButton = new InlineKeyboardButton();
            websiteButton.setText("Website");
            websiteButton.setUrl("https://duckduckgo.com/");
            websiteButton.setCallbackData(ORDER_STATUS.toString());

            buttonsRow.add(websiteButton);

            keyboard.add(buttonsRow);

            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            inlineKeyboardMarkup.setKeyboard(keyboard);

            response.setReplyMarkup(inlineKeyboardMarkup);

        }

        if (callBackData.equalsIgnoreCase(CONTACT_HUMAN.toString())) {
            response.setText("Sure! Push the button(s) down below to chat with a human.");

            // First create the keyboard
            List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();

            //Then we create the buttons' row
            List<InlineKeyboardButton> buttonsRow = new ArrayList<>();

            InlineKeyboardButton customerSupportButton = new InlineKeyboardButton();
            customerSupportButton.setText("Human Support");
            customerSupportButton.setUrl("https://t.me/username");

            buttonsRow.add(customerSupportButton);

            keyboard.add(buttonsRow);

            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            inlineKeyboardMarkup.setKeyboard(keyboard);

            response.setReplyMarkup(inlineKeyboardMarkup);
        }

        return response;
    }
}
