package main.java.com.ffc.bot.states.general;

import main.java.com.ffc.bot.responses.CallBackData;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

import static main.java.com.ffc.bot.responses.CallBackData.CONTACT_HUMAN;
import static main.java.com.ffc.bot.responses.CallBackData.ORDER_STATUS;

public class GeneralTextUpdateStrategy implements GeneralStrategy {

    public static final String unknownOption = "We are sorry, but the command you sent could not be interpreted. Push /help for list of available commands";

    @Override
    public SendMessage getResponse(Update update) {
        String chatId = String.valueOf(update.getMessage().getChatId());

        SendMessage response = new SendMessage();
        response.setChatId(chatId);
        response.setText(unknownOption);

        if (update.getMessage().hasText()) {

            String textReceived = update.getMessage().getText().trim();

            if (textReceived.equalsIgnoreCase("/start")) {

                response.setText("Welcome to ABC, please select the option you are looking for.");

                // First create the keyboard
                List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();

                //Then we create the buttons' row
                List<InlineKeyboardButton> buttonsRow = new ArrayList<>();

                InlineKeyboardButton orderStatus = new InlineKeyboardButton();
                orderStatus.setText("Status");
                orderStatus.setCallbackData(ORDER_STATUS.toString());

                InlineKeyboardButton informationButton = new InlineKeyboardButton();
                informationButton.setText("Information");
                informationButton.setCallbackData(CallBackData.MORE_INFORMATION.toString());

                InlineKeyboardButton contactHumanSupportButton = new InlineKeyboardButton();
                contactHumanSupportButton.setText("Support");
                contactHumanSupportButton.setCallbackData(CONTACT_HUMAN.toString());

                buttonsRow.add(orderStatus);
                buttonsRow.add(informationButton);
                buttonsRow.add(contactHumanSupportButton);

                keyboard.add(buttonsRow);

                InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
                inlineKeyboardMarkup.setKeyboard(keyboard);

                response.setReplyMarkup(inlineKeyboardMarkup);

                return response;

            }
        }

        return response;
    }

}